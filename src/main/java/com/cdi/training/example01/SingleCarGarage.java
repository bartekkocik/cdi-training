package com.cdi.training.example01;

import static java.util.Objects.isNull;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class SingleCarGarage {

    // @Inject
    private Car car;

    @Inject
    public SingleCarGarage(@TurboCharged(numberOfTurbins = 2) Car car) {
        this.car = car;
    }

    @PostConstruct
    private void setup() {
        System.out.println("post construct method called to setup bean ");
        System.out.println("state of car field " + car);
    }

    @PreDestroy
    private void tearDown() {
        System.out.println("pre destroy method called to clean up");
    }

    public String getParkedCarName() {
        if (isNull(car)) {
            throw new IllegalStateException("lack of car instance");
        } else {
            return car.getName();
        }
    }
}

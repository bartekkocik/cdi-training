package com.cdi.training.example01;

import static java.util.Objects.nonNull;

import java.util.StringJoiner;
import java.util.UUID;

import javax.inject.Inject;

public class ChevroletCamaro implements Car {

    @Inject
    private Engine engine;


    @Override
    public String getName() {
        return "camaro";
    }

    @Override
    public boolean haveEngine() {
        return nonNull(engine);
    }

    @Override
    public boolean isEngineWorking() {
        return engine.isWorking();
    }

    @Override
    public UUID getEngineSerialNumber() {
        return engine.getSerialNumber();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ChevroletCamaro.class.getSimpleName() + "[", "]")
                .add("engine=" + engine)
                .toString();
    }
}

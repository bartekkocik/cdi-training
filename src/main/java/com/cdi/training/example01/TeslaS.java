package com.cdi.training.example01;

import java.util.UUID;

import javax.enterprise.inject.Alternative;

@Alternative
public class TeslaS implements Car {

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean haveEngine() {
        return false;
    }

    @Override
    public boolean isEngineWorking() {
        return false;
    }

    @Override
    public UUID getEngineSerialNumber() {
        return null;
    }
}

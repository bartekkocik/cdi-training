package com.cdi.training.example01;

import static java.util.Objects.nonNull;

import java.util.StringJoiner;
import java.util.UUID;

import javax.inject.Inject;

@TurboCharged(numberOfTurbins = 2) // <-- for qualified injection
@Red
public class FordMustang implements Car {

    private Engine engine;

    @Inject
    public FordMustang(Engine engine) {
        this.engine = engine;
    }

    @Override
    public boolean haveEngine() {
        return nonNull(engine);
    }

    @Override
    public boolean isEngineWorking() {
        return engine.isWorking();
    }

    @Override
    public UUID getEngineSerialNumber() {
        return engine.getSerialNumber();
    }

    @Override
    public String getName() {
        return "mustang";
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FordMustang.class.getSimpleName() + "[", "]")
                .add("engine=" + engine)
                .toString();
    }
}

package com.cdi.training.example01;

import java.util.UUID;

public interface Car {

    String getName();

    boolean haveEngine();

    boolean isEngineWorking();

    UUID getEngineSerialNumber();
}

package com.cdi.training.example01;

import java.util.StringJoiner;

import javax.inject.Inject;

public class GarageForTwo {

    @Inject
    private Car car;

    @Inject @Red
    private Car redCar;

    @Override
    public String toString() {
        return new StringJoiner(", ", GarageForTwo.class.getSimpleName() + "[", "]")
                .add("car=" + car)
                .add("redCar=" + redCar)
                .toString();
    }
}

package com.cdi.training.example01;

import java.util.StringJoiner;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
// TODO show two engine UUID example, by changing engine bean scope
//@RequestScoped
public class Engine {

    private UUID serialNumber;

    private boolean isWorking;

    @PostConstruct
    private void setSerialNumber() {
        this.serialNumber = UUID.randomUUID();
    }

    public UUID getSerialNumber() {
        return serialNumber;
    }

    public void startEngine() {
        this.isWorking = true;
    }

    public boolean isWorking() {
        return isWorking;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Engine.class.getSimpleName() + "[", "]")
                .add("serialNumber=" + serialNumber)
                .add("isWorking=" + isWorking)
                .toString();
    }
}

package com.cdi.training.example00;

public class DependentClass {

    private DependencyType dependencyType;

    public DependentClass(DependencyType dependencyType) {
        this.dependencyType = dependencyType;
    }

    public void doAction() {
        dependencyType.action();
    }
}

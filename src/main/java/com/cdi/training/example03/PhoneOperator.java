package com.cdi.training.example03;

import javax.inject.Inject;

/**
 * property consumer
 */
public class PhoneOperator {

    @Inject
    @Number
    private String numberToCall;

    public void callNumber() {
        System.out.println("calling number " + numberToCall);
    }


}

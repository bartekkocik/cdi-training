package com.cdi.training.example03;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

/**
 * property producer
 */
public class PhoneBook {

    @Produces
    @Number
    private String number = "500-123-123";

    @Produces
    //@Number
    private String getNumberInShortForm() {
        return number.replace("-", "");
    }

    private void cleanUp(@Disposes @Number String number) {
        System.out.println("value was disposed " + number);
    }

}

package com.cdi.training.example05;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import static java.util.Arrays.asList;

@Loggable
@Interceptor
@Priority(2010)
public class LoggingInterceptor {

    @Inject
    @ExtendedLogger
    private Logger logger;

    @AroundInvoke
    private Object intercept(InvocationContext ic) throws Exception {
        logger.info("method parameters ", asList(ic.getParameters()).toString());
        try {
            return ic.proceed();
        } finally {
            logger.info("method executed");
        }
    }
}

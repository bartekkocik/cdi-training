package com.cdi.training.example05;

public class BillingService {

    @Loggable
    @ExecutionTime
    public void addExpense(Long amount, Currency currency, String operationTitle) throws InterruptedException {
        System.out.println("intercepted method body execution");
        Thread.sleep(500);
    }
}

package com.cdi.training.example05;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

public class LoggerProducer {

    @Produces
    @ExtendedLogger
    public Logger getLogger(InjectionPoint ip) {
        Logger logger = new Logger();
        logger.setClassName(ip.getMember().getDeclaringClass().getName());
        return logger;
    }
}

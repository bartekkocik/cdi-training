package com.cdi.training.example05;

import javax.enterprise.inject.Vetoed;
import javax.enterprise.inject.spi.InjectionPoint;

import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Instacne of this logger will be produced manually
 * {@link LoggerProducer#getLogger(InjectionPoint)}
 */
@Vetoed
public class Logger {

    private String className;

    public void setClassName(String className) {
        this.className = className;
    }

    public void info(Object... messages) {
        if (isNotBlank(className)) {
            System.out.println(new StringBuilder()
                    .append(className)
                    .append(" ")
                    .append(asList(messages))
                    .toString());
        } else {
            System.out.println(asList(messages));
        }
    }
}

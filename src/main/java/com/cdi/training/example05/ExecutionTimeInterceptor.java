package com.cdi.training.example05;


import org.apache.commons.lang.time.StopWatch;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@ExecutionTime
@Interceptor
@Priority(2020)
public class ExecutionTimeInterceptor {

    @Inject
    @ExtendedLogger
    private Logger logger;

    @AroundInvoke
    private Object intercept(InvocationContext ic) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            return ic.proceed();
        } finally {
            stopWatch.stop();
            logger.info(stopWatch.toString(),"ms");
        }
    }
}

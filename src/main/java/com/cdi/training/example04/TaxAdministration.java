package com.cdi.training.example04;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.EventMetadata;

// urzad skarbowy :)
public class TaxAdministration {

    public void registerIncome(@Observes Income income, EventMetadata eventMetadata) throws InterruptedException {
        // Thread.sleep(5_000);
        System.out.println("++ new income registered by sync notification TaxAdministration " + income);

        System.out.println("eventMetadata.getInjectionPoint()" + eventMetadata.getInjectionPoint());
    }
}

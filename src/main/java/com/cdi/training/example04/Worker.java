package com.cdi.training.example04;

import javax.enterprise.event.Event;
import javax.inject.Inject;

public class Worker {

    @Inject
    private Event<Income> incomeEvent;


    // TODO show how easy is to block thread resource
    public void receiveRevenueAndSendSyncEvent(Income income) {
        System.out.println("new income received, sync will be send");
        incomeEvent.fire(income);
    }

    public void receiveRevenueAndSendAsyncEvent(Income income) {
        System.out.println("new income received, async will be send");
        incomeEvent.fireAsync(income);
    }
}

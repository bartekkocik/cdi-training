package com.cdi.training.example04;

import javax.enterprise.event.Observes;
import javax.enterprise.event.ObservesAsync;

public class LoanBank {

    public void registerSyncIncome(@Observes Income income) {
        System.out.println("++ new income registered by sync notification LoanBank " + income);
    }

    public void registerASyncIncome(@ObservesAsync Income income) {
        System.out.println("++ new income registered by async notification LoanBank " + income);
    }


}

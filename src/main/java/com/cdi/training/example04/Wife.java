package com.cdi.training.example04;

import javax.annotation.Priority;
import javax.enterprise.event.Observes;
import javax.enterprise.event.ObservesAsync;

public class Wife {

    public void registerSyncIncome(
            @Observes @Priority(javax.interceptor.Interceptor.Priority.APPLICATION) Income income) {
        System.out.println("++ new income registered by sync notification Wife " + income);
    }

    public void registerASyncIncome(@ObservesAsync Income income) {
        System.out.println("++ new income registered by async notification Wife " + income);
    }


}

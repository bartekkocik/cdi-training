package com.cdi.training.example04;

import com.cdi.training.example05.Currency;

public class Income {

    private Long incomeValue;
    private Currency currency;

    public Income(Long incomeValue, Currency currency) {
        this.incomeValue = incomeValue;
        this.currency = currency;
    }

    public Long getIncomeValue() {
        return incomeValue;
    }

    @Override
    public String toString() {
        return "Income{" +
                "incomeValue=" + incomeValue +
                ", currency=" + currency +
                '}';
    }
}

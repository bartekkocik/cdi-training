package com.cdi.training.example06;

import javax.inject.Inject;

public class MessageService {

    @Inject
    private MessageProducer messageProducer;

    public void printMessage() {
        System.out.println(messageProducer.produceMessage());
    }
}

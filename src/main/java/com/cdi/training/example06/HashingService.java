package com.cdi.training.example06;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.util.Objects.isNull;

public class HashingService {

    public String calculateHash(Message message) {
        if (isNull(message)) {
            return StringUtils.EMPTY;
        }
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return StringUtils.EMPTY;
        }
        return DatatypeConverter.printHexBinary(message.toString().getBytes()).toUpperCase();
    }
}

package com.cdi.training.example06;

public class Message {

    private String content;

    public String getControlSum() {
        return controlSum;
    }

    public void setControlSum(String controlSum) {
        this.controlSum = controlSum;
    }

    private String controlSum;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                ", controlSum='" + controlSum + '\'' +
                '}';
    }
}

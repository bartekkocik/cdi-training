package com.cdi.training.example06;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

@Decorator
public class MessageProducerDecorator implements MessageProducer {

    @Inject
    private HashingService hashingService;

    @Inject
    @Delegate
    private MessageProducer messageProducer;

    @Override
    public Message produceMessage() {
        Message message = messageProducer.produceMessage();
        String hash = hashingService.calculateHash(message);
        message.setControlSum(hash);
        return message;
    }
}

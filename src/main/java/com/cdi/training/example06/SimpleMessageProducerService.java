package com.cdi.training.example06;

public class SimpleMessageProducerService implements MessageProducer {

    @Override
    public Message produceMessage() {
        Message message = new Message();
        message.setContent("content");
        return message;
    }

}

package com.cdi.training.example06;

public interface MessageProducer {

    Message produceMessage();
}

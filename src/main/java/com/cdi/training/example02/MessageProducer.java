package com.cdi.training.example02;

public interface MessageProducer {

    String producerMessage(String header, String body);
}

package com.cdi.training.example02;

import javax.inject.Inject;
import javax.inject.Named;

public class MessageSender {

    private MessageProducer messageProducer;

    @Inject
    public MessageSender(@Named("simple") MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    public void sendMessage() {
        System.out.println(messageProducer.producerMessage("header", "body"));
    }

    public MessageProducer getMessageProducer() {
        return messageProducer;
    }
}

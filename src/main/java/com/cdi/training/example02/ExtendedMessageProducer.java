package com.cdi.training.example02;

import javax.inject.Named;

public class ExtendedMessageProducer implements MessageProducer {

    @Override
    public String producerMessage(String header, String body) {
        return "Extended message";
    }
}

package com.cdi.training.example02;

import javax.inject.Named;

@Named("simple")
public class SimpleMessageProducer implements MessageProducer {

    @Override
    public String producerMessage(String header, String body) {
        return "Simple message";
    }
}

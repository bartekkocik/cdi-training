package com.cdi.training.example02;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.cdi.training.WeldContainerTest;

public class MessageSenderTest extends WeldContainerTest {

    @Test
    public void shouldSelectNamedMessageProducer() {
        // given // when
        MessageSender messageSender = lookupForObjectInContext(MessageSender.class);

        // then
        messageSender.sendMessage();
        MessageProducer messageProducer = messageSender.getMessageProducer();
        assertTrue(messageProducer instanceof SimpleMessageProducer);
    }
}
package com.cdi.training.example01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.enterprise.inject.literal.NamedLiteral;

import org.junit.jupiter.api.Test;

import com.cdi.training.WeldContainerTest;

public class SingleCarGarageTest extends WeldContainerTest {

    @Test
    public void shouldInjectEngine() {
        // given when
        ChevroletCamaro car = lookupForObjectInContext(ChevroletCamaro.class);

        // then
        assertTrue(car.haveEngine());
        assertFalse(car.isEngineWorking());

        Engine engine = lookupForObjectInContext(Engine.class);
        engine.startEngine();
        assertTrue(car.isEngineWorking());
    }

    @Test
    public void shouldSimulateMoreThanOneRequest() throws InterruptedException {
        // given
        ChevroletCamaro car = lookupForObjectInContext(ChevroletCamaro.class);

        // when then
        System.out.println("First call");
        printCarEngineSerialNumberViaNewThread(car);
        Thread.sleep(1000);
        System.out.println("Second call");
        printCarEngineSerialNumberViaNewThread(car);
    }

    @Test
    public void shouldQualifiesBeanToInjection() {
        // given when
        SingleCarGarage garage = lookupForObjectInContext(SingleCarGarage.class);

        // then
        String parkedCarName = garage.getParkedCarName();
        assertNotNull(parkedCarName);
        assertEquals("mustang", parkedCarName);
    }

    @Test
    public void shouldShowScopeOfEngineWhenTwoInjectionPoints() {
        // given when
        GarageForTwo garage = lookupForObjectInContext(GarageForTwo.class);

        // then
        System.out.println(garage.toString());
    }

    @Test
    public void shouldLookupByInterface() {
        // given when
        Car car = lookupForObjectInContext(Car.class);

        // then
        assertTrue(car instanceof ChevroletCamaro);
    }

    @Test
    private void shouldLookupByInterfaceAndAnnotation() {
        // given when
        Car car = lookupForObjectInContext(Car.class, NamedLiteral.of("Mustang"));

        // then
        assertTrue(car instanceof TurboCharged);
        assertTrue(car instanceof FordMustang);
        assertTrue(car instanceof Car);
    }


    private void printCarEngineSerialNumberViaNewThread(ChevroletCamaro car) {
        new Thread(() -> {
            activateRequestContext();
            System.out.println(car.getEngineSerialNumber());
        }).start();
    }
}
package com.cdi.training.example06;

import com.cdi.training.WeldContainerTest;
import org.junit.jupiter.api.Test;

public class MessageServiceTest extends WeldContainerTest {

    @Test
    public void shouldDecorateMessage() {
        // given
        MessageService messageService = lookupForObjectInContext(MessageService.class);

        // when
        messageService.printMessage();

        // then
        // message was decorated
    }
}
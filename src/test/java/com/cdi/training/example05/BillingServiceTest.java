package com.cdi.training.example05;

import com.cdi.training.WeldContainerTest;
import org.junit.jupiter.api.Test;

public class BillingServiceTest extends WeldContainerTest {

    @Test
    public void shouldInterceptMethod() throws InterruptedException {
        // given
        BillingService billingService = lookupForObjectInContext(BillingService.class);

        // when
        billingService.addExpense(999_999L, Currency.PLN, "some expense");
    }
}
package com.cdi.training.example04;

import org.junit.jupiter.api.Test;

import com.cdi.training.WeldContainerTest;
import com.cdi.training.example05.Currency;

public class WorkerTest extends WeldContainerTest {

    @Test
    public void shouldReceiveSyncEvent() {
        // given
        Worker worker = lookupForObjectInContext(Worker.class);
        Income income = new Income(1_000_000L, Currency.EUR);

        // when
        worker.receiveRevenueAndSendSyncEvent(income);

        // income received
    }

    @Test
    public void shouldReceiveAsyncEvent() {
        // given
        Worker worker = lookupForObjectInContext(Worker.class);
        Income income = new Income(900L, Currency.PLN);

        // when
        worker.receiveRevenueAndSendAsyncEvent(income);

        // income received
    }

    @Test
    public void shouldReceiveAsyncEvent2() {
        // given
        Worker worker = lookupForObjectInContext(Worker.class);
        Income income = new Income(900L, Currency.PLN);

        // when
        worker.receiveRevenueAndSendAsyncEvent(income);

        // income received
    }
}
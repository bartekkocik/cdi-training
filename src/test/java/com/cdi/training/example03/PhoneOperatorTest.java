package com.cdi.training.example03;

import com.cdi.training.WeldContainerTest;
import org.junit.jupiter.api.Test;

public class PhoneOperatorTest extends WeldContainerTest {

    @Test
    public void shouldCallNumber() {
        // given // when
        PhoneOperator phoneOperator = lookupForObjectInContext(PhoneOperator.class);

        // then
        phoneOperator.callNumber();
    }
}
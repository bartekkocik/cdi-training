package com.cdi.training;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.annotation.Annotation;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.BeanManager;

import org.jboss.weld.context.RequestContext;
import org.jboss.weld.context.unbound.UnboundLiteral;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public class WeldContainerTest {

    private static Weld weld;
    private static WeldContainer weldContainer;
    private static BeanManager beanManager;

    @BeforeAll
    public static void setup() {
        weld = new Weld();
        weldContainer = weld.initialize();
        beanManager = weldContainer.getBeanManager();
    }

    @AfterAll
    public static void tearDown() {
        weld.shutdown();
    }

    protected Weld getWeld() {
        return weld;
    }

    protected WeldContainer getWeldContainer() {
        return weldContainer;
    }

    protected static BeanManager getBeanManager() {
        return beanManager;
    }

    protected <T> T lookupForObjectInContext(Class<T> aClass, Annotation... annotation) {
        Instance instance = getWeldContainer()
                .select(aClass, annotation);
        activateRequestContext();
        assertNotNull(instance);
        return (T) instance.get();
    }

    protected void activateRequestContext() {
        RequestContext requestContext = getWeldContainer()
                .select(RequestContext.class, UnboundLiteral.INSTANCE)
                .get();
        if (!requestContext.isActive()) {
            requestContext.activate();
        }
    }
}

# cdi-training
Managed beans, context and context manager
 
conducted by Bartosz Kocik

### Example 0
    -base dependency model 
### Example 1
    -Injection and qualified injection
    -Request scope
### Example 2
    -Named qualifier
### Example 3
    -Property
    -qualified property,
    -producer
    -producer with injection point
### Example 4 
    Events 
    -sync (blocking)
    -async
    -observer with Priority
### Example 5 
    -Interceptor (technical) with priority
### Example 6
    -Decorator 

#TODO
    JSF example with scopes ### request, session, conversation and application scope